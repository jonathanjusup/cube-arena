﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelGenerator))]
public class LevelGeneratorEditor : Editor
{
    //Editor Script for Creating a Level in Inspector
    private LevelGenerator _target;

    private void OnEnable()
    {
        _target = (LevelGenerator)target;
    }

    public override void OnInspectorGUI()
    {
        base.DrawDefaultInspector();

        EditorGUILayout.Space(10);
        if (GUILayout.Button("Generate"))
        {
            _target.GenerateLevel();
        }
        if (GUILayout.Button("Clear"))
        {
            _target.ClearLevel();
        }

        EditorGUILayout.Space(10);
        if (GUILayout.Button("Save"))
        {
            _target.SaveLevelConfig();
        }
    }
}
