﻿using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    /*
     * Bullet Script
     * When Colliding it creates an explosion with another Particle System
     */
    public ParticleSystem particle;
    public List<ParticleCollisionEvent> collisionEvents;
    public ParticleSystem explosionPart;
    public AudioClip HitClip;


    void Start()
    {
        particle = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    void OnParticleCollision(GameObject collision)
    {
        int numCollisionEvents = particle.GetCollisionEvents(collision, collisionEvents);

        int i = 0;
        while (i < numCollisionEvents)
        {
            //Creates Explosion on Collision position
            ParticleSystem temp = Instantiate(explosionPart);
            temp.transform.position = collisionEvents[i].intersection;
            temp.transform.forward = collisionEvents[i].normal;
            explosionPart.Play();
            i++;
            
            //Uses Sound Pool and Plays Sound
            if (SoundManager.Instance.audioSourcePool!= null)
            {
                AudioSource source = SoundManager.Instance.audioSourcePool.ClaimSource(gameObject);
                if (source != null)
                {
                    source.transform.position = temp.transform.position;
                    source.pitch = Random.Range(.9f, 1.1f);
                    source.clip = HitClip;
                    source.loop = false;
                    source.Play();
                }
            }
            
        }
    }
}
