﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CubeManipulation;

public class CubeController : MonoBehaviour
{
    //Controls Cube, Health and Extension

    public AudioClip extensionClip;
    public enum cubeFace { 
        None, 
        East, 
        West, 
        North, 
        South,
        Top,
        Bottom
    };
    private bool extended = true;
    public float extensionT = .1f;
    public cubeFace LockedFace;

    public int ContainerNum = default;
    public int Index = default;

    public GameManager gm;
    public BoxCollider bc;
    public Renderer r;
    public Selection s;

    private int Health;
    public void Start()
    {
        gm = FindObjectOfType<GameManager>();
        bc = GetComponent<BoxCollider>();
        r = GetComponent<Renderer>();
    }
    private void FixedUpdate()
    {
        if(Health <= 0)
        {
            for (int i = 0; i < gm.container[ContainerNum].cube.Count; i++)
            {
                if (gm.container[ContainerNum].cube[i].cube == gameObject)
                    s.InitializeReset(ContainerNum, i);
            }
            
            if(Health <= 0) //If Health was modifierd in InitializeReset and isn't 0, Health doesn't reset to 100
                Health = 30;
        }
    }
    private void OnEnable()
    {
        Health = 30;
    }
    public void Damage()
    {
        Health -= 10;
    }
    
    public cubeFace GetSelectedFace(RaycastHit hitInfo)
    {
        //Gets Selected Face of Cube hit by RayCast and returns a normalized Vector
        Vector3 incomingVector = hitInfo.normal - Vector3.up;
        
        if (incomingVector == new Vector3(0, -1, -1))
            return cubeFace.South;
        if (incomingVector == new Vector3(0, -1, 1))
            return cubeFace.North;
        if (incomingVector == new Vector3(-1, -1, 0))
            return cubeFace.West;
        if (incomingVector == new Vector3(1, -1, 0))
            return cubeFace.East;
        if (incomingVector == new Vector3(0, 0, 0))
            return cubeFace.Top;
        if (incomingVector == new Vector3(0, -2, 0))
            return cubeFace.Bottom;
        return cubeFace.None;
    }
    private Vector3 GetExtendingVector(cubeFace Face)
    {
        //Gets Extending Vector by converting the Face to directional Vector
        if (Face == cubeFace.South)
            return Vector3.back;
        if (Face == cubeFace.North)
            return Vector3.forward;
        if (Face == cubeFace.West)
            return Vector3.left;
        if (Face == cubeFace.East)
            return Vector3.right;
        if (Face == cubeFace.Top)
            return Vector3.up;
        if (Face == cubeFace.Bottom)
            return Vector3.down;
        return Vector3.zero;    //Never gets reached; Has to be there
    }
    public void Extend(cubeFace Face, Vector3 extendingVector, int ContainerNum, int Index)
    {
        //Plays Sound
        if (SoundManager.Instance.audioSourcePool != null && extensionClip != null)
        {
            AudioSource source = SoundManager.Instance.audioSourcePool.ClaimSource(gameObject);
            if (source != null)
            {
                source.transform.position = transform.position;
                source.pitch = Random.Range(1.3f, 1.5f);
                source.volume = 0.6f;
                source.clip = extensionClip;
                source.loop = false;
                source.PlayOneShot(source.clip);
            }
        }
        if (extended) //Cubes have to be fully extended to be able to extend again
        {
            if (extendingVector != Vector3.zero)
            {
                LockedFace = Face;
                extended = false;
                StartCoroutine(Extending(transform, extendingVector, extensionT));
                s.OnChangeCubesLeft(false);
                this.ContainerNum = ContainerNum;
                this.Index = Index;

                LevelGenerator.Cube temp = new LevelGenerator.Cube();
                temp.cube = gameObject;
                temp.cc = this;
                gm.container[ContainerNum].cube.Add(temp);

                transform.SetParent(gm.container[ContainerNum].cubeContainer.transform);
                gm.container[ContainerNum].cube[0].cc.LockedFace = LockedFace; //When Cube extends, it changes its Parent, so the "new 0" of the Pool has no Locked Face
            } else
            {
                Debug.Log("Extension not possible");
            }
            
        }
    }
    public Vector3 CheckIfExtensionPossible(cubeFace Face)
    {
        //Checks, if extension is possible or other Cubes are interfering with the extension Target
        Vector3 extendingVector = GetExtendingVector(Face);
        Vector3 startingPos = transform.position;
        Vector3 targetPos = startingPos + extendingVector;

        for (int i = 0; i < gm.container.Count; i++){
            for (int j = 0; j < gm.container[i].cube.Count; j++){
                if (gm.container[i].cube[j].cube.transform.position == targetPos && gm.container[i].cube[j].cube.activeSelf) //Checks whether a Cube interferes with the target Extension Position or not
                {
                    Debug.Log("VECTOR ZERO");
                    return Vector3.zero;
                }
            }
        }
        return targetPos;
    }
    private IEnumerator Extending(Transform transform, Vector3 targetPos, float requiredTime)
    {
        //Extends Cube to extension Position
        Vector3 currentPos = transform.position;
        float t = 0f;

        while(t < 1)
        {
            t += Time.deltaTime / requiredTime;
            transform.position = Vector3.Lerp(currentPos, targetPos, t);
            yield return null;
        }
        extended = true;
    }
    public void JoinFront(Transform Front)
    {
        transform.position = Front.position;
    }
    private void OnParticleCollision(GameObject collision)
    {
        //If hit by Weapon, it gets damaged
        if(collision.tag == "Weapon")
        {
            if (gameObject != gm.container[ContainerNum].cube[0].cube) //If gameObject isn't Origin
            {
                Damage();
            }
        }
    }
}
