﻿using UnityEngine;

public class Drop : MonoBehaviour
{
    //Drop Script; Drops replace destroyed Cubes; Can be collected
    private Transform origin;
    //[SerializeField] private Rigidbody rb;

    void Start()
    {
        origin = FindObjectOfType<GameManager>().transform;
        transform.localScale = Vector3.one * .5f;

    }
    void FixedUpdate()
    {
        //If drops falls off the Plattforms, it gets teleported to the middle of the Level
        if(transform.position.y < -10)
        {
            transform.position = GetDropPosition(origin.position);
        }
    }
    public void DropAt(Vector3 dropPosition)
    {
        transform.position = dropPosition;
        gameObject.SetActive(true);

        //Experimental Rigidbody instead of Animation
        //Vector3 explosionPos = transform.position + new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f));
        //rb.AddExplosionForce(100f, explosionPos, 2f);
    }
    public Vector3 GetDropPosition(Vector3 origin)
    {
        //If Origin is blocked, Drops will be dropped at a higher position, which is unblocked
        RaycastHit hitInfo;
        Ray ray = new Ray(origin, Vector3.down);
        if (Physics.Raycast(ray, out hitInfo))
        {
            return hitInfo.point + new Vector3(0, 5, 0);
        }
        return origin;
    }
}
