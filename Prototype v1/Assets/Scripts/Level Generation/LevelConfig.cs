﻿using UnityEngine;

[CreateAssetMenu(menuName = "Levels/Level Configs", fileName = "New LevelConfig", order = 1)]
public class LevelConfig : ScriptableObject
{
    //Scriptable Object for saving Levels
    public LevelTile[] levelTiles;

    public void SetLevelTiles(LevelTile[] tiles)
    {
        levelTiles = tiles;
    }
}
