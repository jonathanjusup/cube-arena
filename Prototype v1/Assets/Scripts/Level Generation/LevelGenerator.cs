﻿using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    //Generates Level; Can save Levels on scriptable Objects
    [SerializeField] private int size;
    [SerializeField] private LevelConfig config;
    [SerializeField] private GameObject cubePrefab;

    public struct Cubes //General Information
    {
        public GameObject cubeContainer;
        public List<Cube> cube;
    }
    public struct Cube //More detailed Information
    {
        public GameObject cube;
        public CubeController cc;
    }
    public List<Cubes> cubes;
    public int containerLimit;
    private LevelTile[] _levelTiles;
    
    public void GenerateLevel()
    {
        /*
         * Generates Level by creating Cubes, parented to a Container; 
         * Containers used for visualising Cube Extensions and its increasing List
         */
        containerLimit = size * size;
        cubes = new List<Cubes>();

        _levelTiles = new LevelTile[size * size];
        int ContainerNum = 0;

        //ClearLevel();

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                Cubes temp = new Cubes();
                temp.cubeContainer = new GameObject("CubeContainer");
                temp.cubeContainer.transform.SetParent(transform);
                temp.cubeContainer.transform.position = new Vector3(i, 0, j);

                List<Cube> cube = new List<Cube>();

                Cube temp3 = new Cube();
                temp3.cube = Instantiate(cubePrefab, temp.cubeContainer.transform);
                temp3.cc = temp3.cube.GetComponent<CubeController>();
                temp3.cc.ContainerNum = ContainerNum;
                temp3.cc.Index = -1; //Unique Index for Origin Cubes; Gets ignored by reset Mechanic of Seleciton Script

                cube.Add(temp3);
                temp.cube = cube;

                LevelTile tempTile;
                tempTile.pos = temp.cubeContainer.transform.position;
                tempTile.tileType = cubePrefab;
                _levelTiles[ContainerNum] = tempTile;

                cubes.Add(temp);
                ContainerNum++;
            }
        }
    }
    public void SaveLevelConfig()
    {
        //Saves generated Level
        config.SetLevelTiles(_levelTiles);
    }
    public void ClearLevel()
    {
        //Clears entire Level
        int children = transform.childCount;

        for (int i = 0; i < children; i++)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }
    }

}
