﻿using UnityEngine;

[System.Serializable]
//Just a simple struct, for defining a Level Tile, which can be saved
public struct LevelTile
{
    public Vector3 pos;
    public GameObject tileType;
}
