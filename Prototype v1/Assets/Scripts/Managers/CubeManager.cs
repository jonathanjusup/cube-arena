﻿using UnityEngine;

public class CubeManager : MonoBehaviour
{
    //Manages the Cube Pool; Is a Singleton
    public static CubeManager Instance;
    public GameObject original;
    public CubePool cubePool { get; protected set; }

    private void Awake()
    {
        //Singleton
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);


    }
    private void Start()
    {
        cubePool = gameObject.GetComponent<CubePool>();
    }

}