﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CubeManipulation;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    //Manages the general Game

    public List<LevelGenerator.Cubes> container;
    [SerializeField] private Transform Level;
    [SerializeField] TextMeshProUGUI timer;
    private float roundLength = 300; //5mins
    private void Awake()
    {
        container = new List<LevelGenerator.Cubes>();
        ScanLevel();
    }
    private void Start()
    {
        StartCoroutine(StartRound(roundLength));
    }
    public void ScanLevel()
    {
        //Scans Level and provides it to the Players
        for (int i = 0; i < Level.childCount; i++)
        {
            LevelGenerator.Cubes temp = new LevelGenerator.Cubes();
            temp.cubeContainer = Level.GetChild(i).gameObject;

            temp.cube = new List<LevelGenerator.Cube>();
            LevelGenerator.Cube temp2 = new LevelGenerator.Cube();
            temp2.cube = temp.cubeContainer.transform.GetChild(0).gameObject;
            temp2.cc = temp2.cube.GetComponent<CubeController>();
            temp.cube.Add(temp2);

            container.Add(temp);
        }
    }
    public IEnumerator StartRound(float roundLength)
    {
        //Starts the timer; when timer reaches 0, the Game ends and the Scene changes to Scoreboard
        Debug.Log("Round has started");
        float time = roundLength;

        float minutes;
        float seconds;
        while (time >= 0)
        {
            minutes = Mathf.Floor(time / 60);
            seconds = Mathf.RoundToInt(time % 60);

            timer.text = minutes.ToString("00") + ":" + seconds.ToString("00");
            time--;
            yield return new WaitForSeconds(1f);
        }
        if (time <= 0)
        {
            Debug.Log("Game over");
            ToScoreBoard(); //Change Scene to Scoreboard
        }
        yield return null;
    }
    public void ToScoreBoard()
    {
        //Destroys Singletons, which aren't needed anymore
        Score.Instance.GetStats();
        Destroy(PlayerManager.Instance);
        SceneManager.LoadScene("Scoreboard");
    }
}
