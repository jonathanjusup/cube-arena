﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class HelpManager : MonoBehaviour
{
    //Simple Help Manager Script for Audio and Buttons
    private AudioSource audio;

    void Start()
    {
        audio = GetComponent<AudioSource>();
    }
    public void ToMenu()
    {
        SceneManager.LoadScene("MainMenu");
        audio.PlayOneShot(audio.clip);
    }
    public void ToLobby()
    {
        SceneManager.LoadScene("Lobby");
        audio.PlayOneShot(audio.clip);
    }
}
