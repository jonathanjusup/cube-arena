﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    //Simple Menu Script for Audio and Buttons in the Menu

    private AudioSource audio;
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    public void ToLobby()
    {
        SceneManager.LoadScene("Lobby");
        audio.PlayOneShot(audio.clip);
    }
    public void ToHelp()
    {
        SceneManager.LoadScene("Help");
        audio.PlayOneShot(audio.clip);
    }
    public void Exit()
    {
        Application.Quit();
        audio.PlayOneShot(audio.clip);
    }
}
