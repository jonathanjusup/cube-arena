﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager Instance;
    public struct Players
    {
        public PlayerSlot player;
        public int status;
        public SinputSystems.InputDeviceSlot slot;
    }
    public Players[] players;
    [SerializeField] private Transform PlayerSlotHolder;
    [SerializeField] private TextMeshProUGUI countdown;

    private float time;
    private bool allReady = true;
    public bool isCounting = false;
    public bool gameStart = false;

    //Delegates
    public delegate void ChangeTextDelegate(string newState);
    public ChangeTextDelegate OnChangeText;
    
    private void Awake()
    {
        //Singleton
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        players = new Players[4];
        countdown.enabled = false;
    }
    private void Update()
    {
        SinputSystems.InputDeviceSlot slot = Sinput.GetSlotPress("Join");

        if (slot != SinputSystems.InputDeviceSlot.any) //If Slot is currently empty and available
        {            
            bool alreadyJoined = false;

            for (int i = 0; i < players.Length; i++) //Leaving the Lobby
            {
                if (players[i].player != null)
                {
                    if (players[i].player.playerSlot == slot)
                    {
                        //Complete player Entry Reset
                        alreadyJoined = true;
                        players[i].player.ChangeStatus("EMPTY");
                        players[i].status = 0;
                        players[i].slot = SinputSystems.InputDeviceSlot.any;
                        players[i].player = null; //Needs to exists, so the Slots dont switch, when someone leaves
                        i--;
                    }
                }
            }
            if (!alreadyJoined && !gameStart) //Joining the Lobby
            {
                int emptySlot = GetEmptySlot();
                if (emptySlot > 3)
                {
                    Debug.Log("Max Players of 4 reached");
                    return;
                }
                //this is a new player looking to join, so lets let them!
                GameObject newPlayer = PlayerSlotHolder.GetChild(emptySlot).gameObject;
                newPlayer.GetComponent<PlayerSlot>().playerSlot = slot;
                newPlayer.GetComponent<PlayerSlot>().ChangeStatus("JOINED");

                players[emptySlot].player = newPlayer.GetComponent<PlayerSlot>();
                players[emptySlot].status = 0; //0 stands for Joined
                players[emptySlot].slot = slot;
            }
        }
        if(!gameStart)
            CheckStatus();
    }
    public int GetEmptySlot()
    {
        int emptySlot = 0;
        for (int i = 0; i < players.Length; i++) //Goes trough all joined Players
        {
            if (players[i].player != null)
            {
                emptySlot++;
            } else
            {
                i = players.Length;
            }
        }
        return emptySlot; //If emptySlot == 4, it's not available
    }
    public void CheckStatus()
    {
        /*
         * Checks, whether enough Players are joined and whether all Players are ready to start the Countdown;
         * While the Countdown is running, Players can join and leave, but this will affect the Counddown;
         * In order to start the Game, all Players have to be Ready, also when the Countdown is running, until the Game starts
         */
        int joined = 0;
        int ready = 0;
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].player != null)
                joined++;
        }
        if(joined >= 2) //At least 2 Players joined
        {
            for (int i = 0; i < players.Length; i++)
            {
                if(players[i].player != null && players[i].status == 1) //If Player exists and is ready (primig the Countdown)
                {
                    ready++;
                }
            }
            if(joined == ready) //If all Players are ready
            {
                if (!isCounting) //If Counddown isn't already running
                {
                    allReady = true;
                    time = 5;
                    StartCoroutine(CountDown());
                }
            } else
            {
                if (isCounting)
                {
                    //Stop Countdown
                    countdown.enabled = false;
                    isCounting = false;
                    allReady = false;
                    StopCoroutine(CountDown());
                    Debug.Log("Countdown stopped");
                }
            }
        } else
        {
            if (isCounting)
            {
                //Stop Countdown
                countdown.enabled = false;
                isCounting = false;
                allReady = false;
                StopCoroutine(CountDown());
                Debug.Log("Countdown stopped");
            }
        }

    }
    public IEnumerator CountDown()
    {
        //Runs the Countdown of 5 Seconds, when done, Game starts
        time = 5f;
        countdown.enabled = true;
        isCounting = true;

        Debug.Log("COUNTDOWN STARTED");
        while(time > 0)
        {
            countdown.text = time.ToString();
            time--;
            yield return new WaitForSeconds(1f);
        }
        if(time <= 0 && allReady)
            StartGame();
        yield return null;
    }
    public void StartGame()
    {
        //Starts the Game by loading the Level based on the Player Count
        if (allReady)
        {
            Debug.Log("START GAME");
            int playerCount = 0;
            for (int i = 0; i < players.Length; i++)
            {
                if (players[i].player != null)
                    playerCount++;
            }
            gameStart = true;
            if(playerCount > 0)
            {
                SceneManager.LoadScene(playerCount.ToString() + "Player"); //Loads x-Player Map
            }
        }
    }
}