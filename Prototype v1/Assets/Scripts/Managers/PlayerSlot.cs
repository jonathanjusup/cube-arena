﻿using TMPro;
using UnityEngine;

public class PlayerSlot : MonoBehaviour
{
    /*
     * PlayerSlot Class represents the Player in the Lobby.
     * This Class saves the Player Data, which is needed to
     * instantiate the Player, when the Game starts
     */
    public SinputSystems.InputDeviceSlot playerSlot = SinputSystems.InputDeviceSlot.any;
    private PlayerManager lb;
    private TextMeshProUGUI status;

    void Awake()
    {
        lb = FindObjectOfType<PlayerManager>();
        lb.OnChangeText += ChangeStatus;
        status = GetComponentInChildren<TextMeshProUGUI>();
        status.text = "EMPTY";
    }
    private void Update()
    {
        if (Sinput.GetButtonDown("X", playerSlot)) //If Player wants to ready up
        {
            for (int i = 0; i < lb.players.Length; i++)
            {
                if (lb.players[i].player != null)
                {
                    if (lb.players[i].player.playerSlot == playerSlot)
                    {
                        lb.players[i].player.ChangeStatus("READY");
                        lb.players[i].status = 1;
                    }
                }
            }
        }
        if (Sinput.GetButton("B", playerSlot)) //If Player wants to unready
        {
            for (int i = 0; i < lb.players.Length; i++)
            {
                if (lb.players[i].player != null)
                {
                    if (lb.players[i].player.playerSlot == playerSlot)
                    {
                        lb.players[i].player.ChangeStatus("JOINED");
                        lb.players[i].status = 0;
                    }
                }
            }
            
        }
    }
    public void ChangeStatus(string newState)
    {
        /*
         * Changes the Status of the Player
         * 0 : Joined, but not ready
         * 1 : Joined and Ready
         */
        status.text = newState;
    }
}
