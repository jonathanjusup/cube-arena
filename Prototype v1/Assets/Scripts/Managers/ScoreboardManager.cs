﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreboardManager : MonoBehaviour
{
    //Manages PlayerStats from the ended Game and shows it on the Scoreboard

    public int playerNum;
    public Transform Player;
    public Transform Kills;
    public struct Scoreboard
    {
        public TextMeshProUGUI player;
        public TextMeshProUGUI kills;
    }
    public List<Scoreboard> scoreboards;
    void Start()
    {
        playerNum = Score.Instance.playerStats.Count;
        scoreboards = new List<Scoreboard>();
        for (int i = 0; i < 4; i++)
        {
            if(i < playerNum) //If [i] == PlayerNum, TextMesh can be assigned and set, if not it gets deactivated
            {
                Scoreboard temp = new Scoreboard();
                temp.player = Player.GetChild(i + 1).GetComponent<TextMeshProUGUI>();
                temp.player.text = (Score.Instance.stats[i].playerNum + 1).ToString();
                temp.kills = Kills.GetChild(i + 1).GetComponent<TextMeshProUGUI>();
                temp.kills.text = Score.Instance.stats[i].kills.ToString();
                scoreboards.Add(temp);
            } else //Gets TextMeshes, but deactivates them
            {
                Scoreboard temp = new Scoreboard();
                temp.player = Player.GetChild(i + 1).GetComponent<TextMeshProUGUI>();
                temp.player.gameObject.SetActive(false);
                temp.kills = Kills.GetChild(i + 1).GetComponent<TextMeshProUGUI>();
                temp.kills.gameObject.SetActive(false);
            }
            
        }
    }
    public void ToMenu()
    {
        SceneManager.LoadScene("MainMenu");
        ResetSingletons();
    }
    public void Again()
    {
        SceneManager.LoadScene("Lobby");
        ResetSingletons();
    }
    private void ResetSingletons()
    {
        if(PlayerManager.Instance != null)
            Destroy(PlayerManager.Instance.gameObject);
        if(CubeManager.Instance != null)
            Destroy(CubeManager.Instance.gameObject);
        if(Score.Instance != null)
            Destroy(Score.Instance.gameObject);
    }
    
}
