﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    //Sound Manager as Singleton
    public static SoundManager Instance;
    public AudioSourcePool audioSourcePool { get; protected set; }

    private void Awake()
    {
        //Singleton
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {
        audioSourcePool = gameObject.AddComponent<AudioSourcePool>();
        audioSourcePool.CreateOriginalAudioSource();
    }
}
