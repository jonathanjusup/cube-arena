﻿using UnityEngine;

public class Pause : MonoBehaviour
{
    /*
     * Feature to pause the Game midgame and stops the time; 
     * Players can continue or exit the game by ending it and going to the scoreboard
     */

    SinputSystems.InputDeviceSlot slot = SinputSystems.InputDeviceSlot.any;
    [SerializeField] private GameObject pauseOverlay;
    private GameManager gm;
    public static bool paused = false;

    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        pauseOverlay.SetActive(false);
    }
    void Update()
    {
        //Pauses or unpauses the Game via Time Scale
        if (!paused && slot == SinputSystems.InputDeviceSlot.any)
        {
            SinputSystems.InputDeviceSlot slot = Sinput.GetSlotPress("Join");
        }
        if (Sinput.GetButtonDown("Join", slot))
        {
            if (!paused)
            {
                paused = true;
                pauseOverlay.SetActive(true);
                Time.timeScale = 0;
            } else
            {
                paused = false;
                pauseOverlay.SetActive(false);
                Time.timeScale = 1;
                slot = SinputSystems.InputDeviceSlot.any;
            }
        }
    }
    public void Continue()
    {
        paused = false;
        pauseOverlay.SetActive(false);
        Time.timeScale = 1;
    }
    public void Exit()
    {
        paused = false;
        pauseOverlay.SetActive(false);
        Time.timeScale = 1;
        slot = SinputSystems.InputDeviceSlot.any;
        gm.ToScoreBoard();
    }
}
