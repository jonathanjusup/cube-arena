﻿using System.Collections;
using UnityEngine;

namespace CubeManipulation
{
    public class AlternativeSelection : MonoBehaviour
    {
        /*
        * AlternativeSelection Class is the seconday Feature for Selecting and Manipulating Cubes;
        * Cubes get selected by standing on it;
        * Cubes can be selected and extended one time or continuosly (vertically)
        * Cubes can also be extended horizontally (one time or continuos)
        */
        public SinputSystems.InputDeviceSlot playerSlot;

        private Gridsystem grid;
        private GameManager gm;
        private Selection s;
        private LayerMask layerMask;

        private Transform CurrentSelected;
        private CubeController CurrentSelectedController;
        private int ContainerNum = 0;

        private bool selectionLocked = false;

        void Start()
        {
            grid = FindObjectOfType<Gridsystem>();
            gm = FindObjectOfType<GameManager>();
            s = transform.parent.GetComponentInChildren<Selection>();
            layerMask = LayerMask.GetMask("Cube");
        }
        void Update()
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position, Vector3.down, out hitInfo, 2.5f, layerMask))
            {
                Debug.DrawRay(transform.position, Vector3.down, Color.red);
                var finalPosition = grid.GetNearestPointOnGrid(hitInfo.point);
                if (hitInfo.collider.tag == "Cube")
                {
                    //Changes the selected Container when SelectionLocked is false, AKA Player stops continuos Extension by releasing the Extenstion Button
                    if (!selectionLocked)
                    {
                        for (int i = 0; i < gm.container.Count; i++)
                        {
                            if (hitInfo.collider.transform.parent == gm.container[i].cubeContainer.transform)
                            {
                                ContainerNum = i;
                            }
                        }
                    }
                    //Sets CurrentSelected and CurrentSelectedController to hitinfo
                    CurrentSelectedController = GetCubeController(hitInfo.collider.gameObject);
                    CurrentSelectedController.s = s;
                    CurrentSelected = hitInfo.collider.transform;

                    if (Sinput.GetButton("Fire4", playerSlot) && !Selection.extending && !Pause.paused) //Vertical Extension
                    {
                        Vector3 extendingVector;
                        if (!selectionLocked) //Initial Extension (One time)
                        {
                            extendingVector = CurrentSelectedController.CheckIfExtensionPossible(CurrentSelectedController.GetSelectedFace(hitInfo));
                            Selection.extending = true;
                            InitializeExtension(CubeController.cubeFace.Top, ContainerNum, extendingVector);
                            selectionLocked = true;

                        }
                        else //Continuos Extension
                        {
                            //If selection is locked, the Front Cube is always the new one in the Container, can't be hitinfo
                            CurrentSelected = gm.container[ContainerNum].cube[gm.container[ContainerNum].cube.Count - 1].cube.transform;
                            CurrentSelectedController = gm.container[ContainerNum].cube[gm.container[ContainerNum].cube.Count - 1].cc;
                            CurrentSelectedController.s = s;


                            extendingVector = CurrentSelectedController.CheckIfExtensionPossible(CurrentSelectedController.LockedFace);
                            if (extendingVector != Vector3.zero)
                            {
                                ContinueExtension(ContainerNum, extendingVector);
                                selectionLocked = true;
                            }
                            else //If there is a Cube in the way
                            {
                                Debug.Log("Interference");
                            }
                        }
                    }
                    else if (Sinput.GetButton("Fire5", playerSlot) && !Selection.extending && !Pause.paused) //Horizontal Extension
                    {
                        Vector3 extendingVector;
                        if (!selectionLocked) //Ititial Extension (One time)
                        {
                            extendingVector = CurrentSelectedController.CheckIfExtensionPossible(GetDirectionFacing());
                            if (extendingVector != Vector3.zero)
                            {
                                InitializeExtension(GetDirectionFacing(), ContainerNum, extendingVector);
                                selectionLocked = true;
                            }
                            else //If there is a Cube in the way
                            {
                                Debug.Log("Interference");
                            }
                        }
                        else //Continuos Extension
                        {
                            CurrentSelected = gm.container[ContainerNum].cube[gm.container[ContainerNum].cube.Count - 1].cube.transform;
                            CurrentSelectedController = gm.container[ContainerNum].cube[gm.container[ContainerNum].cube.Count - 1].cc;

                            extendingVector = CurrentSelectedController.CheckIfExtensionPossible(CurrentSelectedController.LockedFace);
                            if (extendingVector != Vector3.zero)
                            {
                                ContinueExtension(ContainerNum, extendingVector);
                                selectionLocked = true;
                            }
                            else //If there is a Cube in the way
                            {
                                Debug.Log("Interference");
                            }
                        }
                    }
                    if ((Sinput.GetButtonUp("Fire4", playerSlot) || Sinput.GetButtonUp("Fire5", playerSlot)) && !Pause.paused)
                    {
                        //Resets SelectionLock, CurrentSelected and CurrentSelectedController
                        selectionLocked = false;
                        CurrentSelected = null;
                        CurrentSelectedController = null;
                    }
                }
            }
        }
        private void InitializeExtension(CubeController.cubeFace Face, int ContainerNum, Vector3 extendingVector)
        {
            if (s.personalPoolEntry.cubes.Count > 0) //Cannot extend without Cubes in personal Pool
            {
                Selection.extending = true;
                s.personalPoolEntry.cubes[0].transform.position = CurrentSelected.position;
                s.personalPoolEntry.cubes[0].SetActive(true);
                if (s.personalPoolEntry.cc[0].gm == null)
                    s.personalPoolEntry.cc[0].Start();
                //Gets Index of Cube, on which the new Cube extends and gives the extending Cube it's Index + 1;
                int Index = CurrentSelectedController.Index + 1;
                s.personalPoolEntry.cc[0].s = s;
                s.personalPoolEntry.cc[0].Extend(Face, extendingVector, ContainerNum, Index); //Extension with new Face

                StartCoroutine(RemoveAfterDelay());

            }
        }
        private void ContinueExtension(int ContainerNum, Vector3 extendingVector)
        {
            if (s.personalPoolEntry.cubes.Count > 0) //Cannot extend without Cubes in personal Pool
            {
                Selection.extending = true;
                s.personalPoolEntry.cubes[0].transform.position = CurrentSelected.position;
                s.personalPoolEntry.cubes[0].SetActive(true);
                if (s.personalPoolEntry.cc[0].gm == null)
                    s.personalPoolEntry.cc[0].Start();
                //Gets Index of Cube, on which the new Cube extends and gives the extending Cube it's Index + 1;
                int Index = CurrentSelectedController.Index + 1;
                s.personalPoolEntry.cc[0].s = s;
                s.personalPoolEntry.cc[0].Extend(gm.container[ContainerNum].cube[0].cc.LockedFace, extendingVector, ContainerNum, Index); //Extension with locked Face

                StartCoroutine(RemoveAfterDelay());
            }
        }
        private CubeController GetCubeController(GameObject Hit)
        {
            /*
             * Takes selected GameObject hit by Raycast and outputs it's Controller Component
             * for additional management and handling.
             */
            for (int i = 0; i < gm.container.Count; i++)
            {
                for (int j = 0; j < gm.container[i].cube.Count; j++)
                {
                    if (Hit == gm.container[i].cube[j].cube)
                        return gm.container[i].cube[j].cc;
                }
            }
            return null; //Never reached
        }
        private IEnumerator RemoveAfterDelay()
        {
            //Waits until Front extended completely, then removes Cube from personal Pool
            yield return new WaitForSeconds(CurrentSelectedController.extensionT + 0.1f);
            Selection.extending = false;

            s.personalPoolEntry.cubes.RemoveAt(0);
            s.personalPoolEntry.cc.RemoveAt(0);
        }
        private CubeController.cubeFace GetDirectionFacing()
        {
            //Gets Direction, which the Player is facing (90° Steps)
            if ((transform.parent.rotation.eulerAngles.y >= 315 && transform.parent.rotation.eulerAngles.y < 360) || (transform.parent.rotation.eulerAngles.y >= 0 && transform.parent.rotation.eulerAngles.y < 45))
            {
                return CubeController.cubeFace.North;
            }
            else if (transform.parent.rotation.eulerAngles.y >= 45 && transform.parent.rotation.eulerAngles.y < 135)
            {
                return CubeController.cubeFace.East;
            }
            else if (transform.parent.rotation.eulerAngles.y >= 135 && transform.parent.rotation.eulerAngles.y < 225)
            {
                return CubeController.cubeFace.South;
            }
            else if (transform.parent.rotation.eulerAngles.y >= 225 && transform.parent.rotation.eulerAngles.y < 315)
            {
                return CubeController.cubeFace.West;
            }
            return CubeController.cubeFace.None;
        }
    }
}

