﻿using CubeManipulation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerController : MonoBehaviour
{
    public SinputSystems.InputDeviceSlot playerSlot;
    public AudioClip pickUpClip;
    public AudioClip deathClip;

    [SerializeField] private Camera playerCamera;
    [SerializeField] private GameObject weapon;
    private Collider colliderSelf;
    private Animator animator;
    private Material material;
    private float fade = 1f;

    [SerializeField] private Selection s;

    public List<PlayerStats> playerstats;
    private PlayerSpawner playerSpawner;
    public Vector3 spawnpoint;
    private FirstPersonController fpsController;

    public CubePool.PoolEntry personalPoolEntry;
    private DropPool dp;
    private PickUpPool pup;
    public bool canPickUp = false;
    public bool isDead = false;

    [SerializeField] public LayerMask layerMask;    

    //Delegates
    public delegate void HitTargetDelegate(Vector3 hitPoint);
    public delegate void MissTargetDelegate();
    public delegate void DamagePlayerDelegate(int source);

    public HitTargetDelegate OnHitTarget;
    public MissTargetDelegate OnMissTarget;
    public DamagePlayerDelegate OnDamagePlayer;

    void Start()
    {
        colliderSelf = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        animator.enabled = true;

        playerstats = new List<PlayerStats>();
        playerSpawner = FindObjectOfType<PlayerSpawner>();
        fpsController = GetComponent<FirstPersonController>();
        
        for (int i = 0; i < PlayerManager.Instance.players.Length; i++)
        {
            if (PlayerManager.Instance.players[i].slot != SinputSystems.InputDeviceSlot.any)
            {
                playerstats.Add(playerSpawner.players[i].GetComponentInChildren<PlayerStats>());
            }
        }
        for (int i = 0; i < 4; i++)
        {
            if(gameObject.layer != 20 + i)
            {
                layerMask |= (1 << 20 + i); //Adds all Player Layers to get ignored by Raycast except own Layer

            }
        }

        dp = FindObjectOfType<DropPool>();
        pup = FindObjectOfType<PickUpPool>();

        material = transform.GetChild(0).GetChild(0).GetComponent<SkinnedMeshRenderer>().material;
        fade = 1f;
        material.SetFloat("_Fade", fade);
    }
    private void Update()
    {
        if (Sinput.GetButtonDown("Fire1", playerSlot) && !Pause.paused && !isDead)
        {
            FireWeapon();
        }
        if(transform.position.y < -20) //Respawns Player, if Player fell off the Platforms
        {
            spawnpoint = playerSpawner.GetFreeSpawnpoint(spawnpoint);
            fpsController.Respawn(spawnpoint);
        }
    }
    private void FireWeapon()
    {
        //Shoots Raycast; If Raycast hits something, FireWeapon with target; If nothing was hit, shoot without a target
        RaycastHit hitInfo;
        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);
        Vector3 direction = ray.direction;

        Debug.DrawRay(ray.origin, ray.direction, Color.yellow, 2);

        if (Physics.Raycast(ray, out hitInfo, 100, layerMask))
        {
            if (hitInfo.collider == colliderSelf)
            {
                ray = new Ray(hitInfo.point + direction * 0.1f, direction);
                if (Physics.Raycast(ray, out hitInfo, 100, layerMask))
                {
                    Debug.Log("New Ray from new Origin");
                    OnHitTarget(hitInfo.point);
                }
                else
                {
                    Debug.Log("Miss");
                    OnMissTarget();
                }
            }
            else
            {
                Debug.Log("Hit");
                OnHitTarget(hitInfo.point);
            }

        }
        else
        {
            Debug.Log("Miss");
            OnMissTarget();
        }
        animator.SetTrigger("_Shoot");
    }
    private void OnParticleCollision(GameObject collision)
    {
        if (collision.tag == "Weapon" && !isDead)
        {
            Debug.Log("HIT FROM BULLET");
            switch (collision.gameObject.layer)
            {
                /*
                 * Checks the Layer of the Weapon, which the Player got hits with 
                 * and damages the Player while providing the Player, who hit him
                 */
                case 28:
                    OnDamagePlayer(0);
                    break;
                case 29:
                    OnDamagePlayer(1);
                    break;
                case 30:
                    OnDamagePlayer(2);
                    break;
                case 31:
                    OnDamagePlayer(3);
                    break;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PickUp") //When Player collides with PickUp
        {
            if (canPickUp)
            {
                //Adds Cube back to personalPoolEntry for use
                personalPoolEntry.cc.Add(pup.pickUps[0]);
                personalPoolEntry.cubes.Add(pup.pickUps[0].gameObject);

                pup.pickUps.RemoveAt(0);
                s.OnChangeCubesLeft(true);
                ResetDrop(other.gameObject);
                
            }
        }
    }
    public IEnumerator Die()
    {
        //Lets Player die; Fade out Player Shader
        animator.enabled = false;
        isDead = true;
        float t = 0f;
        float requiredTime = 2f;
        PlaySound(deathClip);
        weapon.SetActive(false);
        fpsController.m_CharacterController.enabled = false;

        while (t < 1) //Player Fade out
        {
            t += Time.deltaTime / requiredTime;
            fade = 1 - t;
            material.SetFloat("_Fade", fade);
            yield return null;
        }
        StartCoroutine(InitializeRespawn());
        yield return null;
    }
    public IEnumerator InitializeRespawn()
    {
        //Inititalizes Respawn and waits a fixed Amount of Seconds until reseting all Values
        Debug.Log("RESPAWN");
        yield return new WaitForSeconds(3f);
        animator.enabled = true;
        spawnpoint = playerSpawner.GetFreeSpawnpoint(spawnpoint);
        fpsController.Respawn(spawnpoint);

        weapon.SetActive(true);
        fpsController.m_CharacterController.enabled = true;

        fade = 1f;
        material.SetFloat("_Fade", fade);
        isDead = false;
        yield return null;
    }
    public void ResetDrop(GameObject drop)
    {
        //When picked u Drop, it plays Sound and gets reset
        for (int i = 0; i < dp.dropList.Count; i++)
        {
            if(drop.transform.parent.gameObject == dp.dropList[i].gameObject)
            {
                PlaySound(pickUpClip);
                dp.dropList[i].gameObject.SetActive(false);
            }
        }
    }
    private void PlaySound(AudioClip clip)
    {
        //Plays Sounds with assigned Clip
        if (SoundManager.Instance.audioSourcePool != null && clip != null)
        {
            AudioSource source = SoundManager.Instance.audioSourcePool.ClaimSource(gameObject);
            if (source != null)
            {
                source.transform.position = transform.position;
                source.pitch = Random.Range(.9f, 1.1f);
                source.clip = clip;
                source.loop = false;
                source.PlayOneShot(source.clip);
            }
        }
    }
}
