﻿using UnityEngine;
using UnityEngine.UI;
using CubeManipulation;
using TMPro;

public class PlayerStats : MonoBehaviour
{
    private float health;
    private float maxHealth = 100;
    public float cubesLeft;
    public int kills;

    [SerializeField] private Image healthBar;
    [SerializeField] private TextMeshProUGUI cubeAmount;
    [SerializeField] private TextMeshProUGUI killsAmount;

    private Selection s;
    private PlayerController playerController;
    void Awake()
    {
        playerController = GetComponentInParent<PlayerController>();
        s = FindObjectOfType<Selection>();

        playerController.OnDamagePlayer += Damage;
        s.OnChangeCubesLeft += ChangeCubesLeft;

        kills = 0;
    }
    private void Start()
    {
        killsAmount.text = kills.ToString();
        health = maxHealth; ;
        healthBar.fillAmount = health / maxHealth;
    }

    private void ChangeCubesLeft(bool cubeDestroyed)
    {
        //Updates the Cube Amount, the Player has
        if (cubeDestroyed)
        {
            cubesLeft++;
            cubeAmount.text = cubesLeft.ToString();
            Debug.Log("Cubes Left = " + cubesLeft);
        }
        else
        {
            cubesLeft--;
            cubeAmount.text = cubesLeft.ToString();
            Debug.Log("Cubes Left = " + cubesLeft);

        }
    }
    private void FixedUpdate()
    {
        /*
         * Checks, if Player can pick up anymore Cubes.
         * This prevents the Player picking up more Cubes than his Pool Size
         */
        if (cubesLeft < CubePool.cubeLimit)
        {
            playerController.canPickUp = true;
        }
        else
        {
            playerController.canPickUp = false;
        }
    }
    public void KilledEnemy()
    {
        //Ups the Kill Count
        kills++;
        killsAmount.text = kills.ToString();
    }
    private void Damage(int source)
    {
        //Gets Damage from Source; If Source kills Player, it's Kill Count increases
        if(health > 0) //Prevents killing the Player again while he's dying and respawning
        {
            health -= 10;
            healthBar.fillAmount = health / maxHealth;
            if (health <= 0)
            {
                StartCoroutine(playerController.Die());
                playerController.playerstats[source].KilledEnemy();//Ups the kill Amount of the Source Player
                health = maxHealth;
                healthBar.fillAmount = health / maxHealth;
            }
        }   
    }
}
