﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CubeManipulation
{
    /*
     * Selection Class is the primary Feature for Selecting and Manipulating Cubes on sight;
     * Cubes can be selected extended one time or continuosly
     */
    public class Selection : MonoBehaviour
    {
        public SinputSystems.InputDeviceSlot playerSlot;

        private Gridsystem grid;
        private GameManager gm;
        private Camera c;
        private LayerMask layerMask;

        private Transform CurrentSelected;
        private CubeController CurrentSelectedController;
        private int ContainerNum = 0;

        [SerializeField] private PlayerStats stats;

        public static bool extending = false;
        private bool selectionLocked = false;

        //Pools
        public CubePool.PoolEntry personalPoolEntry;
        private Transform pickUpPool;
        private DropPool dp;
        private PickUpPool pup;

        private List<int> Neighbours;

        //Delegates
        public delegate void CubesLeftChangeDelegate(bool cubeDestroyed);
        public CubesLeftChangeDelegate OnChangeCubesLeft;

        void Awake()
        {
            grid = FindObjectOfType<Gridsystem>();
            gm = FindObjectOfType<GameManager>();
            c = transform.parent.GetComponentInChildren<Camera>();
            layerMask = LayerMask.GetMask("Cube");

            //Personal Cube Pool
            CubeManager.Instance.cubePool.ClaimPoolEntry(gameObject);
            personalPoolEntry = CubeManager.Instance.cubePool.GetPoolEntry(gameObject); //Gets List of personal Cubes from Pool
            FindObjectOfType<PlayerController>().personalPoolEntry = personalPoolEntry;
            stats.cubesLeft = personalPoolEntry.cubes.Count;

            pickUpPool = FindObjectOfType<CubeManager>().transform.GetChild(2).transform; //PickUp Pool in Cube Manager
            dp = FindObjectOfType<DropPool>();
            pup = FindObjectOfType<PickUpPool>();
            Neighbours = new List<int>();
        }
        void Update()
        {
            RaycastHit hitInfo;
            Ray ray = c.ViewportPointToRay(new Vector2(.5f, .5f));
            if (Physics.Raycast(ray, out hitInfo, 10f, layerMask))
            {
                var finalPosition = grid.GetNearestPointOnGrid(hitInfo.point);
                transform.position = finalPosition + new Vector3(0, 0, 0);
                Debug.DrawRay(ray.origin, ray.direction * 10, Color.red);
                //Debug.Log(hitInfo.point);
                if (hitInfo.collider.tag == "Cube")
                {
                    //Changes the selected Container when SelectionLocked is false, AKA Player stops continuos Extension by releasing the Extenstion Button
                    
                    if (!selectionLocked)
                    {
                        for (int i = 0; i < gm.container.Count; i++)
                        {
                            if (hitInfo.collider.transform.parent == gm.container[i].cubeContainer.transform)
                            {
                                ContainerNum = i;
                            }
                        }
                    }
                    //Sets CurrentSelected and CurrentSelectedController to hitinfo
                    CurrentSelectedController = GetCubeController(hitInfo.collider.gameObject);
                    CurrentSelectedController.s = this;
                    CurrentSelected = hitInfo.collider.transform;

                    if (Sinput.GetButton("Fire2", playerSlot) && !extending && !Pause.paused)
                    {
                        Vector3 extendingVector;
                        if (!selectionLocked) //Initial Extension (one time)
                        {
                            extendingVector = CurrentSelectedController.CheckIfExtensionPossible(CurrentSelectedController.GetSelectedFace(hitInfo));
                            InitializeExtension(hitInfo, ContainerNum, extendingVector);
                            selectionLocked = true;
                        }
                        else //Continuos Extension
                        {
                            //If selection is locked, the Front Cube is always the new one in the Container, can't be hitinfo
                            CurrentSelected = gm.container[ContainerNum].cube[gm.container[ContainerNum].cube.Count - 1].cube.transform;
                            CurrentSelectedController = gm.container[ContainerNum].cube[gm.container[ContainerNum].cube.Count - 1].cc;
                            CurrentSelectedController.s = this;

                            extendingVector = CurrentSelectedController.CheckIfExtensionPossible(CurrentSelectedController.LockedFace);
                            if (extendingVector != Vector3.zero)
                            {
                                ContinueExtension(ContainerNum, extendingVector);
                                selectionLocked = true;
                            }
                            else //If there is a Cube in the way
                            {
                                Debug.Log("Interference");
                            }
                        }

                    }
                    if (Sinput.GetButtonUp("Fire2", playerSlot) && !Pause.paused)
                    {
                        //Resets SelectionLock, CurrentSelected and CurrentSelectedController
                        selectionLocked = false;
                        CurrentSelected = null;
                        CurrentSelectedController = null;
                    }
                }
            }

        }
        private void InitializeExtension(RaycastHit hitInfo, int ContainerNum, Vector3 extendingVector)
        {
            if (personalPoolEntry.cubes.Count > 0) //Cannot extend without Cubes in personal Pool
            {
                extending = true;
                personalPoolEntry.cubes[0].transform.position = CurrentSelected.position;
                if(!personalPoolEntry.cubes[0].activeSelf)
                    personalPoolEntry.cubes[0].SetActive(true);
                if (personalPoolEntry.cc[0].gm == null)
                    personalPoolEntry.cc[0].Start();
                //Gets Index of Cube, on which the new Cube extends and gives the extending Cube it's Index + 1;
                int Index = CurrentSelectedController.Index + 1;
                personalPoolEntry.cc[0].s = this;
                personalPoolEntry.cc[0].Extend(CurrentSelectedController.GetSelectedFace(hitInfo), extendingVector, ContainerNum, Index); //Extension with new Face

                StopCoroutine(RemoveAfterDelay());
                StartCoroutine(RemoveAfterDelay());
            }
        }
        private void ContinueExtension(int ContainerNum, Vector3 extendingVector)
        {
            if(personalPoolEntry.cubes.Count > 0) //Cannot extend without Cubes in personal Pool
            {
                extending = true;
                personalPoolEntry.cubes[0].transform.position = CurrentSelected.position;
                if (!personalPoolEntry.cubes[0].activeSelf)
                    personalPoolEntry.cubes[0].SetActive(true);
                if(personalPoolEntry.cc[0].gm == null)
                    personalPoolEntry.cc[0].Start();
                //Gets Index of Cube, on which the new Cube extends and gives the extending Cube it's Index + 1;
                int Index = CurrentSelectedController.Index + 1;
                personalPoolEntry.cc[0].s = this;
                personalPoolEntry.cc[0].Extend(gm.container[ContainerNum].cube[0].cc.LockedFace, extendingVector, ContainerNum, Index); //Extension with locked Face

                StopCoroutine(RemoveAfterDelay());
                StartCoroutine(RemoveAfterDelay());
            }
        }
        private IEnumerator RemoveAfterDelay()
        {
            //Waits until Front extended completely, then removes Cube from personal Pool
            yield return new WaitForSeconds(CurrentSelectedController.extensionT + 0.1f);
            extending = false;

            personalPoolEntry.cubes.RemoveAt(0);
            personalPoolEntry.cc.RemoveAt(0);
            yield return null;
        }
        private CubeController GetCubeController(GameObject Hit)
        {
            /*
             * Takes selected GameObject hit by Raycast and outputs it's Controller Component
             * for additional management and handling.
             */
            for (int i = 0; i < gm.container.Count; i++)
            {
                for (int j = 0; j < gm.container[i].cube.Count; j++)
                {
                    if (Hit == gm.container[i].cube[j].cube)
                        return gm.container[i].cube[j].cc;
                }
            }
            return null; //Never reached
        }
        public void InitializeReset(int ContainerNum, int Index)
        {
            //Initializes Reset of Cubes
            selectionLocked = false;
            StartCoroutine(ResetCubes(Index, ContainerNum));
        }
        private IEnumerator ResetCubes(int Index, int ContainerNum)
        {
            /*
             * Resets Cube, checks for acitve Neighbours in the Container
             * Coroutine executes itself recursively, until no Neighbours are found anymore
             */
            Vector3 temp = gm.container[ContainerNum].cube[Index].cube.transform.position;
            if (gm.container[ContainerNum].cube[Index].cube.activeSelf && Index != 0)
            {
                gm.container[ContainerNum].cube[Index].cube.SetActive(false);

                //Put Drop here, instead of Cube
                for (int i = 0; i < dp.dropList.Count; i++)
                {
                    if (!dp.dropList[i].gameObject.activeSelf)
                    {
                        dp.dropList[i].DropAt(gm.container[ContainerNum].cube[Index].cube.transform.position);
                        break;
                    }

                }

                pup.pickUps.Add(gm.container[ContainerNum].cube[Index].cc);
                gm.container[ContainerNum].cube[Index].cube.transform.SetParent(pickUpPool);

                yield return new WaitForSeconds(.1f);
            }
            if (Neighbours.Count == 0)
            {
                //Stops itself, if no Neighbours are found
                StopCoroutine(ResetCubes(Index, ContainerNum));
            }
            if (Index < gm.container[ContainerNum].cube.Count - 1)
            {
                //Check Neighbours in any direction
                CheckNeighbours(ContainerNum, Index, temp); //Returns Amount of Neighbours of Cube
                while (Neighbours.Count > 0) //If there are Neighbours (also Index Neighbours), which are active
                {
                    int tempIndex = Neighbours[Neighbours.Count - 1];
                    Neighbours.RemoveAt(Neighbours.Count - 1);
                    yield return StartCoroutine(ResetCubes(tempIndex, ContainerNum));

                    yield return null;
                }
            }
            extending = false;
            if(Index != 0 && Index < gm.container[ContainerNum].cube.Count && !gm.container[ContainerNum].cube[Index].cube.activeSelf)
            {
                gm.container[ContainerNum].cube.Remove(gm.container[ContainerNum].cube[Index]); //Removes Cube from Container, when Reset done (multiple Execution by this Coroutine)
            }
            yield return null;
        }
        private void CheckNeighbours(int ContainerNum, int Index, Vector3 temp)
        {
            for (int i = 0; Index + i < gm.container[ContainerNum].cube.Count; i++) //Goes trough whole Container; Count can change while Cubes are parallely reset;
            {
                if (gm.container[ContainerNum].cube[Index + i].cube.activeSelf) //Checks only Cubes, which are active
                {
                    bool isNeighbour = true;
                    if (gm.container[ContainerNum].cube[Index].cube.transform.position - gm.container[ContainerNum].cube[Index + i].cube.transform.position != new Vector3(1, 0, 0))
                        if (gm.container[ContainerNum].cube[Index].cube.transform.position - gm.container[ContainerNum].cube[Index + i].cube.transform.position != new Vector3(-1, 0, 0))
                            if (gm.container[ContainerNum].cube[Index].cube.transform.position - gm.container[ContainerNum].cube[Index + i].cube.transform.position != new Vector3(0, 1, 0))
                                if (gm.container[ContainerNum].cube[Index].cube.transform.position - gm.container[ContainerNum].cube[Index + i].cube.transform.position != new Vector3(0, -1, 0))
                                    if (gm.container[ContainerNum].cube[Index].cube.transform.position - gm.container[ContainerNum].cube[Index + i].cube.transform.position != new Vector3(0, 0, 1))
                                        if (gm.container[ContainerNum].cube[Index].cube.transform.position - gm.container[ContainerNum].cube[Index + i].cube.transform.position != new Vector3(0, 0, -1))
                                            isNeighbour = false; //If no Neighbour was found

                    if (isNeighbour && gm.container[ContainerNum].cube[Index + i].cc.Index == gm.container[ContainerNum].cube[Index].cc.Index + 1) //If Neighbour is also Index Neighbour
                    {
                        Neighbours.Add(Index + i); //Adds Neighbour to Neighbour List
                    }
                }
            }
        }
    }
}
