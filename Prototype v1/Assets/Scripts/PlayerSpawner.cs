﻿using CubeManipulation;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerSpawner : MonoBehaviour
{
	private PlayerManager playerManager; //From Lobby with Player Informaiton
	public GameObject playerPrefab;

	//Player Spawnpoints
	public Transform[] Spawnpoints;
	public Transform Spawnpoint1;
	public Transform Spawnpoint2;
	public Transform Spawnpoint3;
	public Transform Spawnpoint4;

	public List<FirstPersonController> players = new List<FirstPersonController>();

	void Start()
	{
		Spawnpoints = new Transform[4] { Spawnpoint1, Spawnpoint2, Spawnpoint3, Spawnpoint4 };
		playerManager = PlayerManager.Instance;

		for (int i = 0; i < playerManager.players.Length; i++)
		{
			if (playerManager.players[i].slot != SinputSystems.InputDeviceSlot.any)
				InitializePlayer(playerManager.players[i].slot);
		}
	}
	public void InitializePlayer(SinputSystems.InputDeviceSlot slot)
	{
		//New Player Initialization
		Vector3 spawnpoint = GetFreeSpawnpoint(Spawnpoints[players.Count].position);
		GameObject newPlayer = Instantiate(playerPrefab, spawnpoint, Quaternion.identity);
		newPlayer.transform.position = Spawnpoints[players.Count].position;

		//Assignes Slots to every important Component, which controls the Player
		newPlayer.GetComponentInChildren<AlternativeSelection>().playerSlot = slot;
		newPlayer.GetComponentInChildren<Selection>().playerSlot = slot;
		var fps = newPlayer.GetComponent<FirstPersonController>();
		fps.playerSlot = slot;
		
		fps.playerSlotDisplay.text = "Player " + (players.Count + 1).ToString();
		
		//Player Controller Initialization
		var playerController = fps.GetComponent<PlayerController>();
		playerController.playerSlot = slot;
		playerController.spawnpoint = Spawnpoints[players.Count].position;
		
		/*
		 * Culling masks for Player Weapons and own Player Mesh
		 * The Player shouldn't be able to see himself.
		 * The Player should only see his own functional Weapon, but not his decorative Weapon
		 * Decorative Weapon is only visible for the other Player
		 */
		fps.camLayer = 19 + players.Count + 1; //Start Player Layer at 20
		fps.weaponLayer = fps.camLayer + 4;
		fps.teamLayer = fps.camLayer;

		fps.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().gameObject.layer = fps.camLayer;
		fps.gameObject.layer = fps.camLayer;

		//For own functional Weapon Mesh
		fps.weapon.transform.GetChild(0).GetComponentInChildren<MeshRenderer>().gameObject.layer = fps.weaponLayer; //1. Mesh of Weapon
		fps.weapon.transform.GetChild(1).GetComponentInChildren<MeshRenderer>().gameObject.layer = fps.weaponLayer; //2. Mesh of Weapon
		fps.weapon.transform.GetChild(2).gameObject.layer = fps.weaponLayer + 4; //For detecting the source when killed
		
		//For own decorative (Prop-)Weapon Mesh
		fps.propWeapon.transform.GetChild(0).GetComponentInChildren<MeshRenderer>().gameObject.layer = fps.teamLayer; //1. Mesh of Weapon
		fps.propWeapon.transform.GetChild(1).GetComponentInChildren<MeshRenderer>().gameObject.layer = fps.teamLayer; //2. Mesh of Weapon

		var cam = newPlayer.GetComponentInChildren<Camera>();
		cam.cullingMask = cam.cullingMask & ~(1 << fps.camLayer); //Self

		//Changes Culling Mask, so Player only can his own functional Weapon and other Players Prop Weapon
		for (int i = 0; i < 4; i++) 
		{
			if (fps.weaponLayer != 24 + i)
			{
				cam.cullingMask = cam.cullingMask & ~(1 << 24 + i);
			}
		}
		players.Add(fps);

		ChangeCameraRect();
		Score.Instance.playerStats.Add(fps.GetComponentInChildren<PlayerStats>()); //Adds the initialized PlayerStats to the global Score
		
		//This isn't necessary, but will prevent people accidentally pressing join twice quickly
		Sinput.ResetInputs(slot);
	}
	public void ChangeCameraRect()
	{
		//Changes Rect Viewport based on PlayerCount
		switch (players.Count)
		{
			case 1:
				Debug.Log("One Player");
				players[0].GetComponentInChildren<Camera>().rect = new Rect(0f, 0f, 1f, 1f); //Full Screen
				break;
			case 2:
				Debug.Log("Two Players");
				players[0].GetComponentInChildren<Camera>().rect = new Rect(0f, 0f, 1f, .5f); //Down Half
				players[1].GetComponentInChildren<Camera>().rect = new Rect(0f, .5f, 1f, .5f); //Up Half

				break;
			case 3:
				Debug.Log("Three Players");
				players[0].GetComponentInChildren<Camera>().rect = new Rect(0f, .5f, .5f, .5f); //Up Left Corner
				players[1].GetComponentInChildren<Camera>().rect = new Rect(.5f, .5f, .5f, .5f); //Up Right Corner
				players[2].GetComponentInChildren<Camera>().rect = new Rect(0f, 0f, 1f, .5f); //Down Half

				MirrorPlayerHUD(1);
				break;
			case 4:
				Debug.Log("Four Players");
				players[0].GetComponentInChildren<Camera>().rect = new Rect(0f, .5f, .5f, .5f); //Up Left Corner
				players[1].GetComponentInChildren<Camera>().rect = new Rect(.5f, .5f, .5f, .5f); //Up Right Corner
				players[2].GetComponentInChildren<Camera>().rect = new Rect(0f, 0f, .5f, .5f); //Down Left Corner
				players[3].GetComponentInChildren<Camera>().rect = new Rect(.5f, 0f, .5f, .5f); //Down Right Corner

				MirrorPlayerHUD(1);
				MirrorPlayerHUD(3);
				break;
		}
	}
	public void MirrorPlayerHUD(int player)
	{
		/*
		 * If 3 - 4 Players are playing, the HUD of the Players on the right Side of the Screen
		 * has to be mirrored for the global Clock. Or else the Clock blocks the HUD.
		 * This Method is only relevant for 3 - 4 Players
		 */
		Canvas HUD;
		RectTransform health;
		RectTransform cubes;
		RectTransform kills;
		
		HUD = players[player].gameObject.transform.GetComponentInChildren<Canvas>();

		//Mirror Health Bar UI
        health = HUD.transform.Find("Health").GetComponent<RectTransform>();
        health.pivot = new Vector2(1, 0);
        health.anchorMin = new Vector2(1, 0);
        health.anchorMax = new Vector2(1, 0);
        health.anchoredPosition3D = new Vector3(-20, 20, 0);

		//Mirror Cubes UI
		cubes = HUD.transform.Find("Cubes").GetComponent<RectTransform>();
        cubes.pivot = new Vector2(1, 1);
		cubes.anchorMin = new Vector2(1, 1);
		cubes.anchorMax = new Vector2(1, 1);
        cubes.anchoredPosition3D = new Vector3(-20, -20, 0);

		//Mirror Kills UI
		kills = HUD.transform.Find("Kills").GetComponent<RectTransform>();
        kills.pivot = new Vector2(1, 1);
        kills.anchorMin = new Vector2(1, 1);
		kills.anchorMax = new Vector2(1, 1);
        kills.anchoredPosition3D = new Vector3(-20, -190, 0);
    }
    public Vector3 GetFreeSpawnpoint(Vector3 InitSpawnpoint)
	{
		//If Initial Spawnpoint is blocked by Object, Spawnpoint.y changes, returnes new Spawnpoint
		RaycastHit hitInfo;
		Ray ray = new Ray(InitSpawnpoint, Vector3.down);
		if(Physics.Raycast(ray, out hitInfo))
		{
			return hitInfo.point + new Vector3(0, 5, 0);
		}
		return Vector3.zero;
	}
}
