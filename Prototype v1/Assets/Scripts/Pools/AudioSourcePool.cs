﻿using UnityEngine;


public class AudioSourcePool : MonoBehaviour
{
    /*
     * Pool for most of the important Sounds;
     * Audio Sources can be claimed, returned to owner and reset
     */
    struct PoolEntry
    {
        public AudioSource audioSource;
        public GameObject assigned;
    }

    [SerializeField] private int capacityLimit = 48;
    [SerializeField] private int initialCapacity = 4;

    private PoolEntry[] poolEntries;
    private AudioSource original;

    private void Start()
    {
        poolEntries = new PoolEntry[0];
        AddSources();
    }
    private void OnEnable()
    {
        InvokeRepeating("FreeFinishedSounds", 1.0f, 1.0f);
    }
    private void OnDisable()
    {
        CancelInvoke("FreeFinishedSounds");
    }
    public void CreateOriginalAudioSource()
    {
        //Only for initial Audio Source Init
        GameObject audioSource = new GameObject("_genericAudioSource");
        original = audioSource.AddComponent<AudioSource>();
        original.transform.SetParent(transform);
    }
    public AudioSource ClaimSource(GameObject owner)
    {
        //Returns claimed Source
        for (int i = 0; i < poolEntries.Length; i++)
        {
            if (poolEntries[i].assigned == null)
            {
                poolEntries[i].assigned = owner;
                return poolEntries[i].audioSource;
            }
        }

        if (!AddSources())
            return null;
        else
            return ClaimSource(owner);
    }
    public void FreeSourcesFor(GameObject owner)
    {   
        //Frees Sources for next owner and stops them, if playing
        for (int i = 0; i < poolEntries.Length; i++)
        {
            if (poolEntries[i].assigned == null)
            {
                if (poolEntries[i].audioSource.isPlaying)
                {
                    poolEntries[i].audioSource.Stop();
                }
                poolEntries[i].assigned = null;
            }
        }
    }
    private void FreeFinishedSounds()
    {
        //Resets Sound Sources, when they have no owner and it's done playing
        int del = 0;

        for (int i = 0; i < poolEntries.Length; i++)
        {
            if (poolEntries[i].assigned != null && !poolEntries[i].audioSource.isPlaying)
            {
                del++;
                poolEntries[i].assigned = null;
            }
        }
    }
    private bool AddSources()
    {
        //Adds more Audio Sources; if capacity is reached resizes Array and adds more Sources
        if (poolEntries.Length + initialCapacity > capacityLimit)
            return false;

        System.Array.Resize(ref poolEntries, poolEntries.Length + initialCapacity);

        for (int i = 0; i < poolEntries.Length; i++)
        {
            if (poolEntries[i].audioSource == null)
            {
                poolEntries[i].audioSource = Instantiate(original, Vector3.zero, Quaternion.identity, transform);
                poolEntries[i].audioSource.transform.SetParent(transform, false);
            }
        }
        return true;
    }
}
