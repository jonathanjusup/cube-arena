﻿using CubeManipulation;
using System.Collections.Generic;
using UnityEngine;

public class CubePool : MonoBehaviour
{
    //Cube Pool for every Player
    public struct PoolEntry
    {
        public List<GameObject> cubes;
        public List<CubeController> cc;
        public GameObject assigned;
    }

    [SerializeField] private Transform parent;
    public int players = 4;
    [SerializeField] public static int cubeLimit = 40;

    public PoolEntry[] poolEntry;

    void Start()
    {
        poolEntry = new PoolEntry[players];
        Initialize();
    }
    public void ClaimPoolEntry(GameObject owner)
    {
        //Claims Pool Entry with no owner and assigns it to the new owner
        for (int i = 0; i < poolEntry.Length; i++)
        {
            if(poolEntry[i].assigned == null)
            {
                poolEntry[i].assigned = owner;
                for (int j = 0; j < poolEntry[i].cc.Count; j++)
                {
                    poolEntry[i].cc[j].s = owner.GetComponent<Selection>();
                }
                return;
            }
        }
    }
    public PoolEntry GetPoolEntry(GameObject owner)
    {
        //Gets Pool Entry and returns it to the owner
        for (int i = 0; i < poolEntry.Length; i++)
        {
            if(poolEntry[i].assigned == owner)
            {
                return poolEntry[i];
            }
        }
        return default;
    }
    private void Initialize()
    {
        //Instantiates x Amount of cubes to x Amount of Player poolEntries
        for (int i = 0; i < poolEntry.Length; i++)
        {
            poolEntry[i].cubes = new List<GameObject>();
            poolEntry[i].cc = new List<CubeController>();
            for (int j = 0; j < cubeLimit; j++)
            {
                poolEntry[i].cubes.Add(Instantiate(CubeManager.Instance.original, Vector3.zero, Quaternion.identity, parent));
                poolEntry[i].cc.Add(poolEntry[i].cubes[j].GetComponent<CubeController>());

                poolEntry[i].cubes[j].SetActive(false);
            }
        }
    }

}
