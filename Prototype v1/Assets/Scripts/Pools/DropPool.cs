﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropPool : MonoBehaviour
{
    //Pool for Drops, which drop, when Cube gets reset

    public List<Drop> dropList;
    [SerializeField] private Transform parent;

    private CubePool cp;
    [SerializeField] GameObject dropPrefab;
    void Start()
    {
        cp = FindObjectOfType<CubePool>();
        int DropAmount = CubePool.cubeLimit * cp.players;

        for (int i = 0; i < DropAmount; i++)
        {
            Drop temp = Instantiate(dropPrefab, parent).GetComponent<Drop>();
            temp.gameObject.SetActive(false);
            dropList.Add(temp);
        }
    }
}
