﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpPool : MonoBehaviour
{
    //Pool for all PickUps, before they get back in the Player Pools
    public List<CubeController> pickUps;

    void Start()
    {
        pickUps = new List<CubeController>();
    }
}
