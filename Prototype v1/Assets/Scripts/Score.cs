﻿using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    //Gets Stats of the Player and provides them to the Scoreboard
    public static Score Instance;
    public List<PlayerStats> playerStats;

    public struct Stats //Important Information for the Scoreboard
    {
        public int playerNum;
        public int kills;
    }
    public List<Stats> stats;
    private void Awake()
    {
        //Singleton
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        playerStats = new List<PlayerStats>();
    }
    public void GetStats()
    {
        //Gets Stats from every Player
        stats = new List<Stats>();
        for (int i = 0; i < playerStats.Count; i++)
        {
            Stats temp = new Stats();
            temp.playerNum = i;
            temp.kills = playerStats[i].kills;
            stats.Add(temp);
        }
    }
    
}
