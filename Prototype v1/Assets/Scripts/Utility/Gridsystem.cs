﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gridsystem : MonoBehaviour
{
    //Grids the whole Level for exact Selection of Cubes on the Grid
    private float space = 1;
    public Vector3 GetNearestPointOnGrid(Vector3 position)
    {
        position -= transform.position; //When offsetting

        int xCount = Mathf.RoundToInt(position.x / space);
        int yCount = Mathf.RoundToInt(position.y / space);
        int zCount = Mathf.RoundToInt(position.z / space);

        Vector3 result = new Vector3(
            xCount * space,
            yCount * space,
            zCount * space
        );

        result += transform.position; //When offsetting
        return result;
    }
    private void OnDrawGizmos()
    {
        //Draws Dots on Grid to visualize the Grid
        Gizmos.color = Color.green;
        for (float x = -10; x < 10; x += space)
        {
            for (float z = -10; z < 10; z += space)
            {
                var point = GetNearestPointOnGrid(new Vector3(x, 0, z));
                Gizmos.DrawSphere(point + new Vector3(0.5f, 0, 0.5f), 0.05f);
            }
        }
    }
}
