﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Weapon : MonoBehaviour
{
    /*
     * Weapon Script; For Shooting only; Collision is modified to ignore some Layers;
     * Uses Delegates of playerController for hitting or missing Targets
     */
    public AudioClip ShootClip;

    private ParticleSystem particle;

    private FirstPersonController fpsController;
    private PlayerController playerController;

    private void Start()
    {
        particle = GetComponentInChildren<ParticleSystem>();
        fpsController = GetComponentInParent<FirstPersonController>();
        playerController = GetComponentInParent<PlayerController>();

        //Loading Delegates for hitting or missing a target
        playerController.OnHitTarget += ShootAndHit;
        playerController.OnMissTarget += ShootAndMiss;

        //Collision Layers to ignore
        var coll = particle.collision;
        coll.collidesWith = coll.collidesWith & ~(1 << fpsController.teamLayer);
        coll.collidesWith = coll.collidesWith & ~(1 << 12); //BulletIgnoreLayer
        coll.collidesWith = coll.collidesWith & ~(1 << 13); //BulletIgnoreLayer Drops
    }
    public void ShootAndHit(Vector3 hitPosition)
    {
        //Shoot with a target; Direction of the weapon doesn't have an influence on the Direction
        particle.transform.LookAt(hitPosition);
        Shoot();
    }
    public void ShootAndMiss()
    {
        //Shoot without a target Position; Just shoots a Bullet in direction the weapon is facing
        particle.transform.localRotation = Quaternion.Euler(Vector3.forward);
        Shoot();
    }
    public void Shoot()
    {
        //Plays Shoot Clip and emits a Particle, which can collide with other Objects
        if (SoundManager.Instance.audioSourcePool != null && ShootClip != null)
        {
            AudioSource source = SoundManager.Instance.audioSourcePool.ClaimSource(gameObject);
            if (source != null)
            {
                source.transform.position = particle.transform.position;
                source.pitch = Random.Range(.9f, 1.1f);
                source.clip = ShootClip;
                source.loop = false;
                source.Play();
            }
        }
        particle.Emit(1);
    }
}